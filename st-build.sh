#!/usr/bin/env bash
set -eou pipefail

# need to call as sudo

tempdir="$(mktemp -d)"
st_dir="$tempdir/st"
patch_dir="$tempdir/patches"

echo "Get st repo..."
git clone git://git.suckless.org/st "$st_dir"

mkdir -p "$patch_dir"
cd "$patch_dir"

echo ""
echo "Get patches..."
curl -O "https://st.suckless.org/patches/xresources/st-xresources-20200604-9ba7ecf.diff"

cd "$st_dir"

patch -i "$patch_dir/st-xresources-20200604-9ba7ecf.diff"

echo ""
echo "Building..."
make clean install

echo ""
echo "Cleanup..."
rm -r "$tempdir"
